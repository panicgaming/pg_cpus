package net.panicgaming.pg_cpus;

public class CPUAccessViolation extends Exception {
    public CPUAccessViolation(String exc) {
        super(exc);
    }

    public CPUAccessViolation(String exc, Throwable throwable) {
        super(exc, throwable);
    }
}
