package net.panicgaming.pg_cpus;

public class CPUMemoryException extends Exception {
    public CPUMemoryException(String exc) {
        super(exc);
    }

    public CPUMemoryException(String exc, Throwable throwable) {
        super(exc, throwable);
    }
}
