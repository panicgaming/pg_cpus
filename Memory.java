package net.panicgaming.pg_cpus;

public class Memory {
    public static Memory fromSizeFactory(int size) 
        throws CPUMemoryException {
        if(size < 0) {
            throw new CPUMemoryException(
                    String.format("Invalid memory size: %d", size)
            );
        }
        else {
            return new Memory(size);
        }
    }

    private Memory(int size) {
        m_memsize = size;
        m_memory = new byte[m_memsize];
    }

    private byte[] m_memory;
    private int m_memsize;
};
