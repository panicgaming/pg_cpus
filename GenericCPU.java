package net.panicgaming.pg_cpus;

/**
 *  An abstract, idealized CPU. 
 *  A CPU Includes the memory, registers and interfaces to other devices.
 */
public abstract class GenericCPU {

    /**
     * Sets a memory location to a specified value
     * @param loc   The location to set
     * @param value The value to set
     * @return  The value that was set
     * @throws CPUAccessViolation when loc is out of bounds
     */
    abstract protected byte setMemoryLoc(int loc, byte value);

    /**
     * Sets a block of memory to specified values
     * @param loc       The start of the block to set
     * @param values    A list of bytes to set
     * @return  The array of values set.
     * @throws CPUAccessViolation when loc is out of bounds or
     *                   if the length of values extends past the end
     *                   of memory.
     */
    abstract protected byte[] setMemoryBlock(int loc, byte[] values);

    /**
     * Reads the value at a specified memory location
     * @param loc   The location to read
     * @return The byte value at loc
     * @throws CPUAccessViolation if loc is out of bounds
     */
    abstract protected byte readMemoryLoc(int loc);

    /**
     * Reads multiple values from a specified memory location
     * @param loc   The start of memory to read from
     * @param amt   The number of bytes to read
     * @return The list of bytes read
     * @throws CPUAccessViolation if loc is out of bounds, or loc + amt
     *              exceeds the memory bounds
     */
    abstract protected byte[] readMemoryBlock(int loc, int amt);


    /**
     * Memory directly accessible by the CPU.
     * This can include sections of memory that link to other devices.
     */
    protected Memory sys_mem;
}
